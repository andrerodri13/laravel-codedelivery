<?php

namespace CodeDelivery\Http\Controllers\Api\Deliveryman;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Authorizer;

class DeliverymanCheckoutController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var OrderService
     */
    private $orderService;
    /**
     * @var Authorizer
     */
    private $authorizer;

    public function __construct(OrderRepository $orderRepository, UserRepository $userRepository, OrderService $orderService, Authorizer $authorizer)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->orderService = $orderService;
        $this->authorizer = $authorizer;
    }

    public function index()
    {
        $id = $this->authorizer->getResourceOwnerId();
        $orders = $this->orderRepository->with(['items'])->scopeQuery(function ($query) use ($id) {
            return $query->where('user_deliverymen_id', '=', $id);
        })->paginate();

        return $orders;
    }

    public function show($id)
    {
        $idDeliveryman = $this->authorizer->getResourceOwnerId();
        return $this->orderRepository->getByIdAndDeliveryman($id, $idDeliveryman);
    }

    public function updateStatus(Request $request, $id)
    {
        $idDeliveryman = $this->authorizer->getResourceOwnerId();
        $order = $this->orderService->updateStatus($id, $idDeliveryman, $request->get('status'));
        if ($order) {
            return $order;
        }
        abort(400, "Ordem não encontrado");


    }


}
