@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Nova Categorias</h3>
        @include('errors._check')

        {!! Form::open(['route' => 'admin.categories.store', 'class' => 'form']) !!}
        @include('admin.categories._form')
        <div class="form-group">

            {!! Form::submit('Criar Categoria', ['class' => 'btn btn-success']) !!}
        </div>


        {!! Form::close() !!}
    </div>
@endsection