@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Pedido: {{$order->id}} - Valor: R$ {{$order->total}}</h2>

        <h3>Cliente: {{$order->client->user->name}}</h3>
        <h4>Data: {{$order->created_at}} </h4>

        <p>
            Entregar em:<br>
            {{$order->client->address}} - {{$order->client->city}} - {{$order->client->state}}
        </p>

        {!! Form::model($order, ['route' => ['admin.orders.update', $order->id], 'method' => 'put']) !!}

        <div class="form-group">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::select('status', $list_status, null,  ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('user_deliverymen_id', 'Entregador:') !!}
            {!! Form::select('user_deliverymen_id',['' => 'Selecione um Entregador'] + $deliverymen->toArray(), null,  ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Salvar Pedido', ['class' => 'btn btn-success']) !!}
        </div>


        {!! Form::close() !!}
    </div>
@endsection